#!/bin/bash

mkdir -p /azp-tasks

# Need a lower version of nodejs than provided
# by image
apt-get install nodejs
pushd /azp-tasks-repo
npm install
for task in $(ls ./Tasks/); do
    nodejs make.js build --task $task
    cp -R /azp-tasks-repo/_build/Tasks/* /azp-tasks/
done;
popd
apt-get remove nodejs
apt-get autoremove

