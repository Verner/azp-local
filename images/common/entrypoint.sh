#!/bin/bash
YELLOW="1;33m"
RED="0;31m"
GREEN="0;32m"
UNDERLINE="4m"
BOLD="1m"
REVERT_COLOR="0m"

function colorize {
    echo "\e[$1 $2 \e[$REVERT_COLOR"
}


cd /src
echo -e "Image: $(colorize $YELLOW "$1")"
echo -e "Converting $(colorize $YELLOW azure-pipelines.yaml) to bash scripts"
JOBS=`/usr/bin/azp2bash.py $@`
echo "#!/bin/bash" > run-pipeline.sh
for job in $JOBS; do
    echo -e "   $(colorize $BOLD "$job")"
    echo "./$job run" >> run-pipeline.sh
done;
chmod a+x run-pipeline.sh
echo "Cleaning build directory /src/build"
rm -rf /src/build
mkdir /src/build
echo 
echo -e "Run $(colorize $YELLOW ./run-pipeline.sh) to run the pipeline"
echo -e "Or cat ./run-pipeline.sh to see the pipeline job scripts"
echo -e "and run them step-by step, list their steps etc."
echo -e "For more options, run a job script with the $(colorize $YELLOW --help) option like, e.g., so"
echo 
echo -e "   $(colorize $YELLOW "$(echo "$JOBS" | head -1) --help")"
echo 
echo -e "Dropping you to an interactive shell"
exec /bin/bash


