#!/usr/bin/env python3

import json
import stat
import sys
import textwrap
import yaml


from pathlib import Path

TASKS_DIR = "/azp-tasks"
ARTIFACTS_DIR = "/artifacts"
SECRETS_DIR = "/secrets"

SCRIPT_HEADER = textwrap.dedent("""
    #!/bin/bash

    SCRIPT_NAME=$(basename $0)
    SCRIPT_VERSION=0.1
    LOG_DIR=/logs
    LOG_FILE=/logs/$SCRIPT_NAME.log
    ARTIFACTS_DIR=/artifacts

    STEPS=""
    declare -A STEPS_NAMES
    declare -A STEPS_TYPES

    function get_step_title {
        name="${STEPS_NAMES[$1]}";
        if [ -z "$name" ]; then
            name=$1
        fi;
        echo "$name (${STEPS_TYPES[$1]})"
    }

    # A helper function which runs the step whose
    # ID is given as argument
    function run_step {
        _TITLE=$(get_step_title "$1");
        _FUNC_NAME="step_$1";
        status_msg "Running step $(colorize $YELLOW "$_TITLE")"
        $_FUNC_NAME 2>&1 | ts '     [%Y-%m-%d %H:%M:%S]' | tee --append $LOG_FILE
        _RET=${PIPESTATUS[0]}
        if [ "$_RET" -ne 0 ]; then
            status_msg "Finished Step $(colorize $YELLOW "$_TITLE" ): $(colorize $RED FAILED)"
        else
            status_msg "Finished Step $(colorize $YELLOW "$_TITLE" ): $(colorize $GREEN OK)"
        fi;
        return $_RET
    }

    function list_steps {
        for step in $1; do
          echo -e "     $step.   $(colorize $YELLOW "$(get_step_title $step)")"
        done
    }

    function print_help {
        echo -e  "$(colorize $YELLOW "$SCRIPT_NAME") v$SCRIPT_VERSION Azure-pipeline jobs converted to bash"
        echo -e  "Created by $(colorize $GREEN azp2bash.py)"
        echo -e
        echo -e  "  $SCRIPT_NAME {list|run} [argument]"
        echo -e
        echo -e  "  $(colorize $BOLD list)      lists the available jobs and their IDs"
        echo -e  "  $(colorize $BOLD run)       run a job; with no arguments runs all the steps in succession"
        echo -e  "              when an argument is given, it should be the ID of the job; passing"
        echo -e  "              a comma separated list of IDs to the --skip option will cause these"
        echo -e  "              jobs to be skipped."
    }

    function in_list {
        _elt=$1
        _list=$2
        [[ $_list =~ (^| )$_elt($| ) ]]
        return $?
    }

    #################################################
    #         CONSOLE COLOR UTILITY FUNCTIONS       #
    #################################################


    YELLOW="1;33m"
    RED="0;31m"
    GREEN="0;32m"
    UNDERLINE="4m"
    BOLD="1m"
    REVERT_COLOR="0m"
    STATUS_COL=10;

    function status_msg {
        echo -e "$@" 1>&2;
        echo -e "$@" >> $LOG_FILE;
    }

    function progress_msg {
        echo -n "$@" 1>&2;
    }

    function status_start {
        msg="$@";
        msgln=`echo "$msg" | wc -c`;
        let ntabs=$STATUS_COL-\(\($msgln-1\)/8\)
        for i in `seq $ntabs`; do
            msg="$msg\t";
        done;
        echo -e -n "$msg" 1>&2;
        echo "" &>> $LOG_FILE
        echo "------------------------------------" &>> $LOG_FILE
        echo "$@" &>> $LOG_FILE
        echo "------------------------------------" &>> $LOG_FILE
    }

    function add_spaces_to_len { # string target_length
        str="$1"
        target_length="$2"
        strlen=`echo "$str" | wc -c`;
        let nspaces=$target_length-$strlen;
        for i in `seq $nspaces`; do
        str="$str ";
        done;
        echo -e "$str";
    }

    function colorize {
        echo "\e[$1 $2 \e[$REVERT_COLOR"
    }

    function status_ok {
        local msg
        if [ ! -z "$1" ]; then
            msg="($1)";
        fi;
        echo -e "\e[$GREEN OK \e[$REVERT_COLOR $msg" 1>&2;
        echo "---------------OK-------------------" &>> $LOG_FILE
    }

    function status_fail {
        echo -e "\e[$RED FAIL \e[$REVERT_COLOR" 1>&2;
        echo "--------------FAIL------------------" &>> $LOG_FILE
    }

    function status_warn {
        echo -e "\e[$YELLOW WARNING: \$@ \e[$REVERT_COLOR"
        echo "---------------WARN-----------------" &>> $LOG_FILE
    }

    function status_err {
        echo -e "\e[$RED ERROR: $@ \e[$REVERT_COLOR"
        echo "---------------ERROR-----------------" &>> $LOG_FILE
    }

    function status_end {
        echo "$@" 1>&2;
        echo "---------------OK-------------------" &>> $LOG_FILE
    }

    function to_upper {
        echo "$@" | tr [:lower:] [:upper:]
    }

    function to_lower {
        echo "$@" | tr [:upper:] [:lower:]
    }

""").strip()

SCRIPT_FOOTER = textwrap.dedent("""
    COMMAND=""
    JOB_LIST=""
    SKIP_LIST="{ENV_SETUP_ID}" # it is automatically run before the other jobs
    while [ $# -gt 0 ] ; do
        case "$1" in
            --help)
                print_help
                exit 0
                ;;
            --skip)
                SKIP_LIST="$SKIP_LIST $1"
                ;;
            *)
                if [ -z "$COMMAND" ]; then
                    COMMAND="$1";
                else
                    JOB_LIST="$JOB_LIST $1";
                fi;
                ;;
        esac;
        shift;
    done

    if [ -z "$COMMAND" ]; then
        COMMAND="run"
    fi;

    if [ -z "$JOB_LIST" ]; then
        JOB_LIST=$STEPS
    fi;

    case $COMMAND in
        list)
            list_steps "$STEPS"
            exit 0
            ;;
        run)
            step_{ENV_SETUP_ID} # always run environment setup
            for job in $JOB_LIST; do
                if ! in_list $job $SKIP_LIST; then
                    run_step $job
                fi;
            done;
            ;;
        *)
            print_help
            exit 1
            ;;
    esac;
""").strip()

PIPELINE_VARS = {
    '$(System.DefaultWorkingDirectory)': "/src",
    '$(Build.SourcesDirectory)': "/src",
    "$(Agent.TempDirectory)":"/tmp",
    "$(Agent.workFolder)":"/src",
}

VARIABLE_GROUPS = {
}

def normalize_name(nm):
    return nm.replace(' ','_').replace('.', '_').upper()

def resolve_name(task, nm):
    """
        Task input variables may have different aliases. This function
        uses the task.json file to resolve alias names to the canonical
        variable names.
    """
    ret = nm
    for inp in task.get('inputs', []):
        if nm in inp.get('aliases', []) or nm == inp.get('name', None):
            return inp.get('name', nm)
    return nm

def eval_substs(val):
    """
        Does variable substitutions (e.g. $(System.DefaultWorkingDirectory) => '/src')
        on provided val.
    """
    for var, var_val in PIPELINE_VARS.items():
        val = str(val).replace(var, var_val)
    return val


def handle_task(step):
    try:
        task_dir = Path(f"{TASKS_DIR}/{step['task'].replace('@','V')}")
        task_info = json.loads((task_dir/'task.json').read_text())

        env = []
        for name, val in step['inputs'].items():
            val = eval_substs(val)
            env.append(f"""INPUT_{normalize_name(resolve_name(task_info, name))}="{val}" """)
        for name, val in PIPELINE_VARS.items():
            name = name[2:-1]  # Strip $()
            env.append(f"""{normalize_name(name)}="{val}" """)

        task_script = task_info['execution']['Node']['target']
        return f'{" ".join(env)} node {task_dir}/{task_script}'
    except Exception as ex:
        return f"""echo -e "Task was not successuflly converted $(colorize $RED "{ex}")"\nreturn 1;\n""" + textwrap.indent("Error: processing task\n"+str(ex), '# ')


def handle_checkout(step):
    return textwrap.dedent("""
            echo "Git checkout not supported, repo is mounted into the image as /src"
            # Git checkout not supported, repo will be mounted into the image
            # This is what azure does:
            #
            # git init $SRC_DIR
            # pushd $SRC_DIR
            # git remote add origin $REPO_URL
            # git config gc.auto 0
            # git config --get-all http.$REPO_URL.extraheader
            # git config --get-all http.proxy
            # git -c http.extraheader="AUTHORIZATION: bearer ***" fetch --force --tags --prune --progress --no-recurse-submodules origin
            # git checkout --progress --force $COMMIT_ID
            # git submodule sync --recursive
            # git -c http.https://technologynexus.visualstudio.com.extraheader="AUTHORIZATION: bearer ***" submodule update --init --force --recursive
            # git config http.$REPO_URL.extraheader "AUTHORIZATION: bearer ***"
            # popd
            """)


def handle_script(step):
    return eval_substs(step['script'].strip())


def handle_publish(step):
    return textwrap.dedent(f"""mv {step['publish']} $ARTIFACTS_DIR/{step['artifact']}""")

def handle_download(step):
    artifact = eval_substs(step['inputs']['artifact'])
    path = eval_substs(step['inputs']['downloadPath'])
    # The '*' is there, because input doesn't specify the 7z/zip extensions...
    return textwrap.dedent(f"""cp $ARTIFACTS_DIR/{artifact}* {path}""")

def handle_secure_download(step):
    path = eval_substs(step['inputs']['secureFile'])
    PIPELINE_VARS[f"$({step['name']}.secureFilePath)"] = f"{SECRETS_DIR}/{path}"
    return textwrap.dedent(f"""
                                if [ -f '{SECRETS_DIR}/{path}' ]; then
                                    echo 'Secure file'
                                    echo '  {path}'
                                    echo 'will be located at'
                                    echo '  {SECRETS_DIR}/{path} (${step['name'].upper()}_SECUREFILEPATH)'
                                else
                                    echo 'Secure file {SECRETS_DIR}/{path} not found'
                                    return 1
                                fi;""")

def handle_variables(vars):
    ret = []
    err = False
    for grp in vars:
        if 'group' in grp:
            g_name = grp['group']
            ret.append(f"echo Importing variable group {g_name}")
            if g_name not in VARIABLE_GROUPS:
                ret.append(f"""echo $(colorize $RED "Failed"), no such group!""")
                err = True
            else:
                for key, val in VARIABLE_GROUPS[g_name].items():
                    ret.append(f"echo '  {key} = {val['value']}'")
                    PIPELINE_VARS[f"$({key})"] = val['value']
        elif 'name' in grp:
            ret.append(f"echo 'Setting variable {grp['name']}'")
            ret.append(f"echo '    {grp['name']} = {grp['value']}'")
            PIPELINE_VARS[f"$({grp['name']})"] = grp['value']
        else:
            ret.append(f"echo 'Setting variable {grp}'")
            ret.append(f"echo '    {grp} = {vars[grp]}'")
            PIPELINE_VARS[f"$({grp})"] = vars[grp]

    if err:
        ret.append("return 1")
    return textwrap.dedent("\n".join(ret))

def environment_setup():
    ret = []
    for var, val in PIPELINE_VARS.items():
        ret.append(f"{var[2:-1].upper().replace('.','_')}='{val}'")
    return textwrap.dedent("\n".join(ret))

def step2bash(step, n):
    if 'checkout' in step:
        tp = 'Checkout sources'
        src = handle_checkout(step)
    elif 'init' in step:
        tp = 'Init step'
        src = step['src']
    elif 'script' in step:
        tp = 'Bash Script'
        src = handle_script(step)
    elif 'task' in step and 'DownloadSecureFile' in step['task']:
        # Special case for handling secret files
        tp = 'Download Secret Files'
        src = handle_secure_download(step)
    elif 'task' in step and 'DownloadPipelineArtifact' in step['task']:
        # Special case for handling artifact downloads
        tp = 'Download Artifacts'
        src = handle_download(step)
    elif 'task' in step:
        tp = 'Azure Pipeline Task: '+step['task']
        src = handle_task(step)
    elif 'publish' in step:
        tp = 'Publish Artifacts'
        src = handle_publish(step)
    else:
        tp = "Unknown type"
        src = f"""echo -e "Task was not successuflly converted: $(colorize $RED "unknown task type")"\nreturn 1;\n""" + textwrap.indent(json.dumps(step, indent=4), '# ')
    src = textwrap.indent(src, '    ')
    return f"""
# ==========================================================
# Step:   {step.get('displayName','') or ''}
# Type:   {tp}
# Number: {n}
# ----------------------------------------------------------
STEPS_NAMES[{n}]="{step.get('displayName')}"
STEPS_TYPES[{n}]="{tp}"
STEPS="$STEPS {n}"
function step_{n} {{
{src}
}}
# ==========================================================
"""

filter_image = sys.argv[1]
pipelines = yaml.safe_load(open('azure-pipelines.yaml', 'r'))
jobs = [j for j in pipelines['jobs'] if filter_image == j['pool']['vmImage']]

# Load variables
secret_vars = json.load(open(f'/{SECRETS_DIR}/variables.json', 'r'))
for grp in secret_vars:
    VARIABLE_GROUPS[grp['name']] = grp['variables']

init_steps = []
if 'variables' in pipelines:
    init_steps.append({
        'init': True,
        'src': handle_variables(pipelines['variables']),
        'displayName': 'Init per-pipeline variables'
    })

for job in jobs:
    if 'variables' in job:
        init_steps.append({
            'init': True,
            'src': handle_variables(job['variables']),
            'displayName': 'Init per-job variables'
        })
    init_steps.append({
        'init': True,
        'src': environment_setup(),
        'displayName': 'Setup Environment'
    })
    env_setup_id=len(init_steps)-1
    job['steps'] = init_steps + job['steps']
    out_name = f"run-{job['job']}.sh"
    print(out_name)
    with open(out_name, 'w') as OUT:
        OUT.write(SCRIPT_HEADER+"\n")
        for (n, step) in enumerate(job['steps']):
            OUT.write(step2bash(step, n))
        OUT.write(SCRIPT_FOOTER.format(ENV_SETUP_ID=env_setup_id))
    out_path = Path(out_name)
    out_path.chmod(out_path.stat().st_mode | stat.S_IEXEC)
