build-orig-ubuntu-1804:
	/opt/hashicorp/packer/bin/packer build -timestamp-ui -except=azure-arm -var-file=./.packer-vars.json ./external/virtual-environments/images/linux/ubuntu1804.json

build-azp-vsphere-win-2019:
	/opt/hashicorp/packer/bin/packer build -timestamp-ui -except=azure-arm -var-file=./.packer-vars.json ./external/virtual-environments/images/win/Windows2019-Azure.json

build-azp-local-ubuntu-1804:
	docker build . -f images/ubuntu-1804/Dockerfile -t `cat images/ubuntu-1804/Dockerfile | grep Name | sed -e's/# Name:\s*//g' | head -1`:`cat images/ubuntu-1804/Dockerfile | grep Version | sed -e's/# Version:\s*//g' | head -1 | tr -d ' '`
