#!/bin/bash

REPO=$1
IMAGE=$2
FORCE=$3

ERROR=""

AZP_DIR=$REPO/.azp-local
ARTIFACT_DIR=$AZP_DIR/artifacts
SECRETS_DIR=$AZP_DIR/secrets
LOG_DIR=$AZP_DIR/logs

# Create a directory for artifacts & logs & shared dirs
mkdir -p $ARTIFACT_DIR
mkdir -p $LOG_DIR
mkdir -p $SECRETS_DIR

YELLOW="1;33m"
RED="0;31m"
GREEN="0;32m"
UNDERLINE="4m"
BOLD="1m"
REVERT_COLOR="0m"

function colorize {
    echo "\e[$1 $2 \e[$REVERT_COLOR"
}

# Create an overlay directory over repo so that
# the azure pipeline image can access it and modify it but the changes do not
# propagate outside
upper=`mktemp -t -d docker-XXX`
merged=`mktemp -t -d merged-XXX`
overlay=`mktemp -t -d overlay-XXX`
work=`mktemp -t -d work-XXX`
sudo mount -t overlay $overlay -o lowerdir=$REPO,upperdir=$upper,workdir=$work $merged/
rm -f $AZP_DIR/src-docker
ln -s $merged $AZP_DIR/src-docker

# Check that secrets are available and bail out with an error if not
if cat azure-pipelines.yaml | grep -q 'group:'; then
    if [ ! -f $SECRETS_DIR/variables.json ]; then
        if [ -f $HOME/.config/azp-local/variables.json ]; then
            cp $HOME/.config/azp-local/variables.json $SECRETS_DIR;
        else
            echo -e ""
            echo -e "$(colorize $BOLD Error:) Pipeline requires secret variables but none present in"
            echo -e ""
            echo -e "        $(colorize $YELLOW $SECRETS_DIR/variables.json)"
            echo -e "or"
            echo -e "        $(colorize $YELLOW $HOME/.config/azp-local/variables.json)"
            echo -e ""
            echo -e "You can create the file using the following command"
            echo -e ""
            echo -e "        \$ $(colorize $YELLOW "az pipelines variable-group list --project PROJECT_NAME > $SECRETS_DIR/variables.json")"
            echo -e ""
            echo -e "Replacing $(colorize $YELLOW PROJECT_NAME) with the name of your project in Azure. You can also save it to"
            echo -e "$(colorize $YELLOW $HOME/.config/azp-local/variables.json) which will be used as a fallback for all projects."
            ERROR="TRUE"
        fi;
    fi;
fi;

SECRET_FILES=$(cat azure-pipelines.yaml | grep 'secureFile:' | sed -e's/[^:]*:\s*//g')
MISSING_FILES=""
for f in $SECRET_FILES; do
    if [ ! -f $SECRETS_DIR/$f ]; then
        if [ -f $HOME/.config/azp-local/secrets/$f ]; then
            cp $HOME/.config/azp-local/secrets/$f $SECRETS_DIR/f;
        else
            MISSING_FILES="$MISSING_FILES $f";
        fi;
    fi;
done;
if [ ! -z "$MISSING_FILES" ]; then
    echo -e ""
    echo -e "$(colorize $BOLD Error:) The following secret files required by the Pipeline are missing";
    echo -e ""
    for f in $MISSING_FILES; do
        echo -e "        $(colorize $YELLOW $f)"
    done;
    echo -e ""
    echo -e "Please download them and save them to $(colorize $YELLOW $SECRETS_DIR) or $(colorize $YELLOW $HOME/.config/azp-local/secrets/)".
    ERROR="TRUE"
fi;


if [ "z$ERROR" == "zTRUE" -a -z "$FORCE" ]; then
    echo -e ""
    echo -e "$(colorize $BOLD "Exiting due to errors.") "
    echo -e " You can force continuation by providing $(colorize $YELLOW --force) as the last argument on the commandline."
    echo -e ""
    exit 1
fi;


echo -e "Artifact directory:        $(colorize $YELLOW $ARTIFACT_DIR)"
echo -e "Secrets directory:         $(colorize $YELLOW $SECRETS_DIR)"
echo -e "Log directory:             $(colorize $YELLOW $LOG_DIR)"
echo -e "Docker repo overlay:       $(colorize $YELLOW $AZP_DIR/src-docker)"
echo -e "Pipeline script in docker: $(colorize $YELLOW /src/run-pipeline.sh)"


# Run the image with the overlay mounted at /src/ inside
docker run -it -v "$merged/:/src/" -v "$ARTIFACT_DIR/:/artifacts/:ro" -v "$SECRETS_DIR/:/secrets/:ro" -v "$LOG_DIR/:/logs/" $IMAGE

# Cleanup
echo "Cleaning up"
echo "sudo umount $merged"
sudo umount $merged
echo "sudo rm -rf $upper $merged $overlay $work"
sudo rm -rf $upper $merged $overlay $work
echo "rm $AZP_DIR/src-docker"
rm $AZP_DIR/src-docker


